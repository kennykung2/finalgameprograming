using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        //[SerializeField] private AudioClip playerFireSound;
        //[SerializeField] private AudioClip playerDestroyerSound;
        //[SerializeField] private float playerFireSoundVolume = 0.1f;
        //[SerializeField] private float playerDestroyerSoundVolume = 0.1f;
        

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            //Debug.Assert(playerFireSound !=null, "playerFireSound cannot be null");
            //Debug.Assert(playerDestroyerSound !=null, "playerDestroyerSound cannot be null");
            //audioSource = gameObject.GetComponent<AudioSource>();
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            //AudioSource.PlayClipAtPoint(playerFireSound,Camera.main.transform.position,playerFireSoundVolume);
            SoundManager.Instance.Play(audioSource,SoundManager.Sound.PlayerFire);
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
        }
        
        

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            //AudioSource.PlayClipAtPoint(playerDestroyerSound,Camera.main.transform.position,playerDestroyerSoundVolume);
            SoundManager.Instance.Play(audioSource,SoundManager.Sound.PlayerDestroy);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}