﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class UI : MonoBehaviour
{
   public void QuitGame()
   {
      Debug.Log("QUIT!");
      Application.Quit();
   }

   public void Back()
   {
      Debug.Log("back!");
      SceneManager.LoadScene("Game");
      
   }
}
